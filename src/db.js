import firebase from "firebase/compat/app";
    import "firebase/compat/database";
import { initializeApp } from "firebase/app";
import { writable, get } from 'svelte/store';
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAXU3aSV3Qs2-XJBtyuywC0Fa8hmNJ5mqI",
    authDomain: "projectdolfeen.firebaseapp.com",
    databaseURL: "https://projectdolfeen-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "projectdolfeen",
    storageBucket: "projectdolfeen.appspot.com",
    messagingSenderId: "631182256388",
    appId: "1:631182256388:web:455d758cc7f4a19ab98851"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
export const db = firebase.database();

export const databaseError = writable(null);
export const userId = writable(null);
export const battleCounter = writable(0);
export const winCounter = writable(0);
export const loseCounter = writable(0);
export const lastMoves = writable([null, null]); // [My move, Opponent move]
export const awaitingBattle = writable(false);
export const ongoingBattle = writable(false)
export const opponent = writable(null);
export const recording = writable(false);
export const audioCompareList = writable([]);
export const soundReplicationSubject = writable(null);
let soundReplicationTarget = null;

const logError = ({ message, level = 'Error', only_log = false }) => {
    if (!only_log) {
        databaseError.set(message);
    }
    if (message !== null) {
        console.log(`${level}: ${message}`);
    }
};


// Try to login the user
addEventListener('load', () => {
    userId.set(localStorage.getItem('userId'));
    if (get(userId)){

        // User statistic change listener
        db.ref('users/' + get(userId)).on('value', userUpdateHandler);
    }
});

// Register function
export const register = async id => {

    logError({ message: null });
    let dbUserRef = db.ref('users/' + id);
    await dbUserRef.once('value', async snapshot => {

        // Check if user already exists
        let current = snapshot.val();
        if (current == null){

            // Create user
            userId.set(id);
            localStorage.setItem('userId', id);
            await dbUserRef.set({ 'id': id, 'history': [] }).then(() => {

                // User statistic change listener
                db.ref('users/' + get(userId)).on('value', userUpdateHandler);
            });
            logError({
                message: 'Creating new user with ID ' + id,
                level: 'Success',
                only_log: true})
            return;
        }


        // Start battle
        if (get(userId) && get(userId) != id){
            requestBattle(id);
        } else {
            logError({ message: 'Trying to start battle with yourself' });
        }
    });
};

// User update handler
const userUpdateHandler = dataSnapshot => {
    let userData = dataSnapshot.val();

    // If user invalid, log out and reload page
    if (userData == null){
        logError({ message: 'Invalid user. Logging out.' });
        localStorage.setItem('userId', '');
        return;
    }

    // Set awaitBattle back to false if requestBattle has been removed
    if (!userData.requestBattle){
        if (get(awaitingBattle)) {
            awaitingBattle.set(false);
            ongoingBattle.set(true);
            lastMoves.set(userData.lastMove)
        }
    }



    // Check if user should create a recording
    if (userData.recordingSetTarget){

        db.ref('users/' + userData.recordingSetTarget + '/recordingSet').once('value', snapshot => {
            let data = snapshot.val();
            soundReplicationTarget = userData.recordingSetTarget;
            soundReplicationSubject.set(data.subject);

            // Start recording audio
            recording.set(true);
            mediaRecorder.start();

            // Push audioData to array
            mediaRecorderChunks = [];
            mediaRecorder.addEventListener("dataavailable", async event => {
                mediaRecorderChunks.push(event.data);
            });

            // Set stop listener
            mediaRecorder.addEventListener("stop", async () => {
                recording.set(false);

                // Convert blot to text
                const blob = new Blob(mediaRecorderChunks, { type: 'audio/webm' });
                let data = await blob.text();

                // Stop the recorder
                mediaRecorder.stop();
                recording.set(false);
                await db.ref('users/' + soundReplicationTarget + '/recordingSet/audio/' + get(userId)).set(data);
                await db.ref('users/' + get(userId) + '/recordingSetTarget').set(null);
                mediaRecorderChunks = [];
                soundReplicationSubject.set(null);
                soundReplicationTarget = null;
            });

            // After 5 seconds
            setTimeout(async () => {
                mediaRecorder.stop();
            }, 5000);
        });
    }

    // Check if user has a completed recordingset
    if (userData.recordingSet && userData.recordingSet.audio && Object.keys(userData.recordingSet.audio).length == 2){

        // Move the set to the admins list
        const uploadFunction = async (userId, dataObject) => {
            await db.ref('recordingSets').once('value', async snapshot => {
                let recordingSets = snapshot.val();
                if (!recordingSets) { recordingSets = []; }
                recordingSets.push(dataObject);
                await db.ref('recordingSets').set(recordingSets);
            });
            db.ref('users/' + userId + '/recordingSet').set(null);
        };
        uploadFunction(userData.recordingSet);
    }

    // Display user data
    let history = userData.history || [];

    battleCounter.set(history.length);

    let winAmount = history.filter(e => e.won).length
    winCounter.set(winAmount);
    loseCounter.set(history.length - winAmount);
};


// Start a battle with a certain team
export const requestBattle = teamId => {

    // Get and validate own team
    if (get(userId)){
        let dbUserRef = db.ref('users/' + get(userId));
        dbUserRef.once('value', snapshot => {
            let userData = snapshot.val();

            // Get and validate opponent team
            let dbTeamRef = db.ref('users/' + teamId);
            dbTeamRef.once('value', snapshot => {
                let teamData = snapshot.val();
                if (teamData){

                    // Check if team has a pending request and that is us
                    if (teamData.battleRequest && teamData.battleRequest == get(userId)){

                        // Accept the request
                        db.ref('users/' + teamData.id + '/battleRequest').set(null);
                        logError({
                            message: `Accepting from ID ${get(userId)} to ID ${teamData.id}`,
                            level: 'Info',
                            only_log: true})
                        acceptBattle(userData, teamData);
                    }

                    // Otherwise, start a battle request
                    else {
                        db.ref('users/' + get(userId) + '/battleRequest').set(teamData.id);
                        awaitingBattle.set(true);
                        opponent.set(teamData.id)
                        logError({
                            message: `Requesting battle from ID ${get(userId)} to ID ${teamData.id}`,
                            level: 'Info',
                            only_log: true})


                        // Remove the request after 20 seconds, if it hasn't already
                        setTimeout(removeBattleRequest, 20000);
                    }
                }
            });
        });
    }
};


// Accept and start a battle
const acceptBattle = (userData, opponentData) => {
    ongoingBattle.set(true)

    // Pick 2 random rock paper scissors
    let userRPS = Math.floor(Math.random() * 4) % 3;
    let opponentRPS = Math.floor(Math.random() * 4) % 3;

    // Determine who won (0=draw, 1=user, 2=opponent)
    let result = 1;
    if (userRPS == opponentRPS) { result = 0; }
    if (userRPS == 0 && opponentRPS == 1) { result = 2; }
    if (userRPS == 1 && opponentRPS == 2) { result = 2; }
    if (userRPS == 2 && opponentRPS == 3) { result = 2; }

    let options = ["Steen", "Papier", "Schaar"];

    // Set last moves
    db.ref('users/' + userData.id + '/lastMove').set([options[userRPS], options[opponentRPS]]);
    db.ref('users/' + opponentData.id + '/lastMove').set([options[opponentRPS], options[userRPS]]);
    lastMoves.set([options[userRPS], options[opponentRPS]]);

    // If there is a winner, store the result
    if (result > 0){

        // Push to history. If no history yet, create initial array.
        db.ref('users/' + userData.id + '/history').once('value', snapshot => {
            let data = snapshot.val();
            if (!data) { data = []; }
            data.push({'opponent': opponentData.id, 'won': result == 1});
            db.ref('users/' + userData.id + '/history').set(data);
        });
        db.ref('users/' + opponentData.id + '/history').once('value', snapshot => {
            let data = snapshot.val();
            if (!data) { data = []; }
            data.push({'opponent': opponentData.id, 'won': result == 2});
            db.ref('users/' + opponentData.id + '/history').set(data);
        });


        // Reset battle target
        db.ref('users/' + userData.id + '/battleRequest').set(null);
        db.ref('users/' + opponentData.id + '/battleRequest').set(null);
    }

    // Otherwise, let the users replicate a sound
    else {
   console.log('starting battle');
        db.ref('replicationSounds').once('value', async snapshot => {
            let data = snapshot.val();

            // Pick a random sound to replicate
            let replicationSound = data.subjects[Math.floor(Math.random() * data.subjects.length) % data.subjects.length];
            if (Math.floor(Math.random() * 4) == 0){
                replicationSound += ', ' + data.modifiers[Math.floor(Math.random() * data.modifiers.length) % data.modifiers.length];
            }
            soundReplicationSubject.set(replicationSound);

            // Create recording objects for both users
            await db.ref('users/' + userData.id + '/recordingSet').set({'subject': get(soundReplicationSubject)});
            await db.ref('users/' + opponentData.id + '/recordingSetTarget').set(userData.id);


            // Start recording audio
            recording.set(true);
            mediaRecorder.start();

            // Push audioData to array
            mediaRecorderChunks = [];
            mediaRecorder.addEventListener("dataavailable", async event => {
                mediaRecorderChunks.push(event.data);
            });

            // When recording stops, submit to database
            mediaRecorder.addEventListener("stop", async () => {
                recording.set(false);

                // Convert blot to text
                const blob = new Blob(mediaRecorderChunks, { type: 'audio/webm' });
                let data = await blob.text();

                // Write data to db
                await db.ref('users/' + get(userId) + '/recordingSet/audio/' + get(userId)).set(data);
            });

            // Set a timeout to stop after 5 seconds
            setTimeout(() => {

                // Stop the recorder
                mediaRecorder.stop();
            }, 5000);
        });
    }
};

// Remove pending request from the user
const removeBattleRequest = () => {
    awaitingBattle.set(false);
    opponent.set(null)

    // Get and validate own data
    if (get(userId)){

        let dbUserRef = db.ref('users/' + get(userId)).once('value', snapshot => {
            let userData = snapshot.val();
            if (userData && userData.battleRequest){
                db.ref('users/' + get(userId) + '/battleRequest').set(null);
            }
        });
    }
};

// Set a listener to recordingSounds
db.ref('recordingSounds').on('value', async snapshot => {
    let out = [];
    let data = snapshot.val();
    for (var row of data){
        
        let newRow = [];
        let dat = [];
        for (var key in row){
            if (key == 'sound'){ newRow.push(row[key]); }
            else { dat.push([key, row[key]]); }
        }
        newRow.push(dat);
        out.push(newRow);
    }
    audioCompareList.set(out);
});


// Create an audio stream recorder
let mediaRecorder = null;
let mediaRecorderChunks = [];
if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

    // Get audio media
    navigator.mediaDevices.getUserMedia({audio: true}).then((stream) => {

        // Create recorder object
        mediaRecorder = new MediaRecorder(stream);

    }).catch((err) => {
        logError({ message: 'Could not get audio data.' });
    });
  } else {
    logError({ message: 'Audio data retrieving is not supported for this device.' });
  }
