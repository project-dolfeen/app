// requires:
//      https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js
//      https://www.gstatic.com/firebasejs/8.1.1/firebase-database.js

    // Firebase config
    const firebaseConfig = {
        apiKey: "AIzaSyAXU3aSV3Qs2-XJBtyuywC0Fa8hmNJ5mqI",
        authDomain: "projectdolfeen.firebaseapp.com",
        databaseURL: "https://projectdolfeen-default-rtdb.europe-west1.firebasedatabase.app",
        projectId: "projectdolfeen",
        storageBucket: "projectdolfeen.appspot.com",
        messagingSenderId: "631182256388",
        appId: "1:631182256388:web:455d758cc7f4a19ab98851"
    };

    // Initialize app and database
    firebase.initializeApp(firebaseConfig);
    const db = firebase.database();
    let userId = null;


    const elId = id => document.getElementById(id);
    let userStatsElement = elId('userStats');
    let errorsElement = elId('errors')
    let battleCounter = elId('battles');
    let winCounter = elId('won');
    let loseCounter = elId('lost');

    // Try to login the user
    addEventListener('load', () => {
        userId = localStorage.getItem('userId')
        if (userId){

            // User statistic change listener
            db.ref('users/' + userId).on('value', userUpdateHandler);
        }
    });

    // Register function
    const register = id => {
        let dbUserRef = db.ref('users/' + id);
        dbUserRef.once('value', snapshot => {

            // Check if user already exists
            let current = snapshot.val();
            if (current == null){

                // Create user
                userId = id;
                localStorage.setItem('userId', userId);
                dbUserRef.set({ 'id': id, 'history': [] }).then(() => {

                    // User statistic change listener
                    db.ref('users/' + userId).on('value', userUpdateHandler);
                });
            }
        });
    };

    // User update handler
    const userUpdateHandler = dataSnapshot => {
        let userData = dataSnapshot.val();
        userStatsElement.style.display = 'none';

        // If user invalid, log out and reload page
        if (userData == null){
            errorsElement.textContent = 'Invalid user. Logging out and reloading the page';
            localStorage.setItem('userId', '');
            setTimeout(() => location.reload(), 2000);
            return;
        }

        // Display user data
        userStatsElement.style.display = 'block';
        let history = userData.history || [];
        let winCount = history.filter(e => e.won).length;
        let loseCount = history.length - winCount;
        battleCounter.textContent = history.length;
        winCounter.textContent = winCount;
        loseCounter.textContent = loseCount;
    };

    /*
    // Start a battle with a certain team
    const requestBattle = teamId => {

        // Get and validate own team
        if (userId){
            let dbUserRef = db.ref('users/' + userId);
            dbUserRef.once('value', snapshot => {
                let userData = snapshot.val();

                // Get and validate opponent team
                let dbTeamRef = db.ref('users/' + teamId);
                dbTeamRef.once('value', snapshot => {
                    let teamData = snapshot.val();
                    if (teamData){

                        // Check if team has a pending request and that is us
                        if (teamData.battleRequest && teamData.battleRequest == userId){

                            // Accept the request
                            acceptBattle(userData, teamData);
                        }

                        // Otherwise, start a battle request
                        else {
                            console.log('battle with ' + teamData.id + ' requested');
                            db.ref('users/' + userId + '/battleRequest').set(teamData.id);

                            // Remove the request after 10 seconds, if it hasnt already
                            setTimeout(removeBattleRequest, 10000);
                        }
                    }
                });
            });
        }
    };

    // Accept and start a battle
    const acceptBattle = (userData, opponentData) => {

        // Pick 2 random rock paper scissors
        let userRPS = Math.floor(Math.random() * 4) % 3;
        let opponentRPS = Math.floor(Math.random() * 4) % 3;

        // Determine who won (0=draw, 1=user, 2=opponent)
        let result = 1;
        if (userRPS == opponentRPS) { result = 0; }
        if (userRPS == 0 && opponentRPS == 1) { result = 2; }
        if (userRPS == 1 && opponentRPS == 2) { result = 2; }
        if (userRPS == 2 && opponentRPS == 3) { result = 2; }

        // If there is a winner, store the result
        if (result > 0){
            db.ref('users/' + userData.id + '/history').push({'opponent': opponentData.id, 'won': result == 1});
            db.ref('users/' + opponentData.id + '/history').push({'opponent': userData.id, 'won': result == 2});
            db.ref('users/' + userData.id).set('battleRequest', null);
            db.ref('users/' + opponentData.id).set('battleRequest', null);
        }
    };

    // Remove pending request from the user
    const removeBattleRequest = () => {

        // Get and validate own data
        if (userId){
            console.log(userId);
            let dbUserRef = db.ref('users/' + userId).once('value', snapshot => {
                let userData = snapshot.val();
                console.log(userData);
                if (userData && userData.battleRequest){
                    db.ref('users/' + userId + '/battleRequest').set(null);
                }
            });
        }
    };
    */


module.exports = { register }